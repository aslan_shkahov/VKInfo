package com.example.vkinfo;

import androidx.test.rule.ActivityTestRule;
import androidx.test.runner.AndroidJUnit4;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.action.ViewActions.typeText;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withSubstring;

@RunWith(AndroidJUnit4.class)
public class MainTest {
    @Rule
    public ActivityTestRule<MainActivity> activityActivityTestRule = new ActivityTestRule<>(MainActivity.class);
    @Test
    public void clickSearchField(){
        onView(withId(R.id.et_search_field)).perform(typeText("1")).check(matches(isDisplayed()));
        onView(withId(R.id.b_search_vk)).perform(click());
        onView(withId(R.id.tv_result)).check(matches(isDisplayed())).check(matches(withSubstring("Фамилия: Durov")));
    }

    @Test
    public void clickSearchField2(){
        onView(withId(R.id.et_search_field)).perform(typeText("2")).check(matches(isDisplayed()));
        onView(withId(R.id.b_search_vk)).perform(click());
        onView(withId(R.id.tv_result)).check(matches(isDisplayed())).check(matches(withSubstring("Фамилия: Vladimirova")));
    }

    @Test
    public void clickSearchField3(){
        onView(withId(R.id.et_search_field)).perform(typeText("5")).check(matches(isDisplayed()));
        onView(withId(R.id.b_search_vk)).perform(click());
        onView(withId(R.id.tv_result)).check(matches(isDisplayed())).check(matches(withSubstring("Фамилия: Perekopsky")));
    }
}
